# Projeto: Jogo da Velha com Arduino

## Alunos

| Nome             | Número USP   |
| -----------------|:------------:|
| Felipe Gandra Moreira   | 14569550     |
| Allan Vitor de Souza Silva      | 14712657     |
| Artur Oliveira Arraes     | 14745532     |
| Matheus Aparecido de Almeida Rodriguez | 14597868     |

## Descrição Geral

Este projeto implementa um jogo da velha utilizando uma matriz de LEDs e botões conectados a um microcontrolador Arduino. O objetivo é permitir que dois jogadores joguem o clássico jogo da velha em um tabuleiro físico, com indicações visuais dos movimentos e do vencedor.

Componentes do Projeto:
- Arduino Uno: O microcontrolador que gerencia toda a lógica do jogo.
- LEDs:
- LEDs Vermelhos (9): Representam as casas do tabuleiro de jogo.
- LED Verde (1): Indica o vencedor ou empate.
- Matriz de Botões (3x3): Utilizados pelos jogadores para realizar os movimentos no tabuleiro.
- Protoboard e Jumpers: Para montar o circuito.

## Funcionamento

### Vídeo sobre o Funcionamento
https://youtu.be/3ALYdQaC1xM  

### LED e Tabuleiro
Cada LED vermelho representa as posições da matriz do tabuleiro do jogo da velha. O LED 1 representa a casa 1, o LED 2 representa a casa 2, e assim por diante até o LED 9, que representa a casa 9. A representação dos jogadores é feita da seguinte forma:

- **Jogador X:** LED com luz permanente.
- **Jogador O:** LED piscante.

### Jogadas
Para realizar as jogadas, os jogadores pressionam os botões da matriz de botões, que estão associados a uma matriz representando o tabuleiro. Por exemplo:

- A casa 1 está na linha 0 e coluna 0. Logo, o botão 1, que também está na linha 0 e coluna 0, é seu representante.

A matriz do jogo é representada da seguinte maneira:

```
0,0  0,1  0,2
1,0  1,1  1,2
2,0  2,1  2,2
```

Os LEDs são organizados assim:

```
Led1 Led2 Led3
Led4 Led5 Led6
Led7 Led8 Led9
```

O tabuleiro é representado por uma matriz inicializada da seguinte forma:

```
Tabuleiro = {'', '', '',
             '', '', '',
             '', '', ''}
```

### Detecção de Vencedor
Quando há um vencedor, o LED 10 (verde) reage de acordo com o vencedor:

- **X vence:** O LED 10 permanece ligado.
- **O vence:** O LED 10 pisca.
- **Empate:** O jogo deve ser reiniciado.

### Conexões dos LEDs
Os LEDs são conectados a um GND comum e suas respectivas saídas de voltagem. As saídas dos LEDs são:

```
{Led1, Led2, Led3, Led4, Led5, Led6, Led7, Led8, Led9}
{8, 9, 10, 11, 12, 13, A0, A1, A2}
```

O LED 10 está conectado ao GND e ao pino A3.

### Inicialização e Detecção dos Botões
Cada saída dos LEDs é inicialmente configurada com 0 V (Modo LOW). Os botões da matriz de botões são responsáveis por ativar essas saídas. Eles são conectados aos pinos de linha e coluna:

- **Linhas:** {2, 3, 4}
- **Colunas:** {5, 6, 7}

As colunas são iniciadas com voltagem (modo HIGH).

### Loop do Programa
Durante o loop do programa, são usados laços para verificar qual botão foi pressionado. Se um botão é pressionado, ele não libera mais DDP (modo LOW), permitindo que o programa detecte a pressão do botão. Uma vez detectado o botão e o jogador, o caractere correspondente (X ou O) é colocado na matriz do tabuleiro.

### Verificação de Vitória
O vencedor é verificado quando o loop de checagem de vitória detecta as seguintes condições:

```cpp
// Checagem de linhas
if (tabuleiro[linha][0] == simbolo && tabuleiro[linha][1] == simbolo && tabuleiro[linha][2] == simbolo) {

// Checagem de colunas
} else if (tabuleiro[0][coluna] == simbolo && tabuleiro[1][coluna] == simbolo && tabuleiro[2][coluna] == simbolo) {

// Checagem de diagonais
} else if ((tabuleiro[0][0] == simbolo && tabuleiro[1][1] == simbolo && tabuleiro[2][2] == simbolo) ||
           (tabuleiro[0][2] == simbolo && tabuleiro[1][1] == simbolo && tabuleiro[2][0] == simbolo)) {
```

Com essas condições, o programa consegue identificar o vencedor e acionar o LED 10 de acordo com o resultado do jogo.

### Simulador
![Simulador](Imagens/Simulador.png)

https://www.tinkercad.com/things/kKfs47iJ2th-jogodavelha?sharecode=hVDV282r8YJZOepgGiMukDTb2w651yXVHqSx42Gp4Y0
